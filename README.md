# Matrix Homeserver on fly.io

Quick notes on how to run [dendrite] for a small scale Matrix homeserver on [fly.io] with sqlite storage.

## Requirements

- A domain name where you can create/change A, AAAA and SRV records
- A [fly.io] account and the [`flyctl`] cli installed
- Docker or similar container runtime installed

## Preparations

For federation (talking to other homeservers), your server needs is a matrix server key.

To generate this server key, the dendrite container image can be used.

```
docker run --rm -it -v $(pwd):/key -w /key --entrypoint /usr/bin/generate-keys matrixdotorg/dendrite-monolith:v0.3.11 --private-key matrix_key.pem
```

**Do not loose this key!**

Next, make copy the `dendrite-example.yaml` to `dentrite.yaml` and change the `global.server_name` to your desired domain.

Finally, change the  `app = "dendrite-on-fly"` line in `fly.toml` to an app name of your desire.

## Deployment

The deployment is using a persistent volume mount, so we need to create the volume before we can deploy.

The following command will create a 10 GB volume in the `fra` region. (A list of available regions can be obtained using `flyctl platform regions`)

```
flyctl volumes create dendrite_data --region fra --size 10
```

Now you simply can deploy dendrite using

```
flyctl deploy
```

## Domain configuration

After deployment, execute `flyctl info` to obtain the IP addresses your app runs on.

Use these IPs to create A and AAAA records for the domain name you configured in `dendrite.yaml`

Usually matrix federates on port 8448, but it is possible to use a SRV record to specify a different port (8443 in our case).

Create a SRV record at `_matrix._tcp.<the-original-domain>` with values `10 10 8443 <the-original-domain>`

It is possible to test federation with the [Matrix federation tester](https://federationtester.matrix.org/).

Once you have set up the A and AAAA records, obtain a TLS certificate using `flyctl`

```
flyctl certs add <hostname>
```

You now can create accounts on your homeserver and start chatting with people.

## Still to figure out

- How to backup your sqlite file

## Useful information

- flyctl v0.0.181 and dendrite v0.3.11 were used

[dendrite]: https://github.com/matrix-org/dendrite
[fly.io]: https://fly.io
[`flyctl`]: https://github.com/superfly/flyctl/releases
